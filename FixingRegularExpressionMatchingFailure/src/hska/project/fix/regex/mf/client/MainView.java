package hska.project.fix.regex.mf.client;

import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;

import hska.project.fix.regex.mf.parser.Alternative;
import hska.project.fix.regex.mf.parser.Concatenation;
import hska.project.fix.regex.mf.parser.Epsilon;
import hska.project.fix.regex.mf.parser.Primitive;
import hska.project.fix.regex.mf.parser.RegEx;
import hska.project.fix.regex.mf.parser.Repetition;

public class MainView {
	
	private EventHandler _eventHandler;
	private TextBox _regularExpressionTextBox;
	private TextBox _wordTextBox;
	private Label _matches;
	private TextBox _fixedTextBox;
	private Tree _initalAST;
	private Tree _fixedAST;
	private ScrollPanel _initialTreeWrapper;
	private ScrollPanel _fixedTreeWrapper;
	private TextArea _textArea;
	private Image _image;
	
	public MainView(BlurHandler blurHandler) {
		_eventHandler = blurHandler;
	}
	
	public void init() {
		_regularExpressionTextBox = new TextBox();
		_regularExpressionTextBox.setSize("100%", "100%");
//		_regularExpressionTextBox.setText("Enter a regular expression");
		
		_wordTextBox = new TextBox();
		_wordTextBox.setSize("100%", "100%");
//		_wordTextBox.setText("Enter a string for matching");
		
	    _image = new Image();
		
		_fixedTextBox = new TextBox();
		_fixedTextBox.setSize("100%", "100%");
		_fixedTextBox.setEnabled(false);
//		_fixedTextBox.setText("No fix available");
		
		_regularExpressionTextBox.addBlurHandler((BlurHandler) _eventHandler);
		_wordTextBox.addBlurHandler((BlurHandler) _eventHandler);
		
		_regularExpressionTextBox.addKeyPressHandler((KeyPressHandler)_eventHandler);
		_wordTextBox.addKeyPressHandler((KeyPressHandler)_eventHandler);
		
		RootPanel.get("textfieldRegEx").add(_regularExpressionTextBox);
		RootPanel.get("textfieldText").add(_wordTextBox);
		RootPanel.get("fixRegExfieldText").add(_fixedTextBox);
	    RootPanel.get("match").add(_image);
		
		createTreeView();
	}

	private void createTreeView() {
		// Create a static tree and a container to hold it
	    _initalAST = new Tree();
	    _initalAST.addTextItem("Empty Abstract Syntax Tree");
	    _initalAST.setAnimationEnabled(true);
	    _initialTreeWrapper = new ScrollPanel(_initalAST);
	    _initialTreeWrapper.setSize("400px", "400px");
	    // Wrap the static tree in a DecoratorPanel
	    DecoratorPanel initialDecorator = new DecoratorPanel();
	    initialDecorator.setWidget(_initialTreeWrapper);
	    
	    // Create a static tree and a container to hold it
	    _fixedAST = new Tree();
	    _fixedAST.addTextItem("Empty Fixed Abstract Syntax Tree");
	    _fixedAST.setAnimationEnabled(true);
	    _fixedTreeWrapper = new ScrollPanel(_fixedAST);
	    _fixedTreeWrapper.setSize("400px", "400px");
	    // Wrap the static tree in a DecoratorPanel
	    DecoratorPanel fixedDecorator = new DecoratorPanel();
	    fixedDecorator.setWidget(_fixedTreeWrapper);
	    
	    _textArea = new TextArea();
	    _textArea.setWidth("100%");
	    _textArea.setHeight("400px");
	    _textArea.setReadOnly(true);
	    // Wrap the static tree in a DecoratorPanel
	    DecoratorPanel textAreaDecorator = new DecoratorPanel();
	    textAreaDecorator.setWidget(_textArea);
	    
	    Grid grid = new Grid(1, 3);
	    grid.getColumnFormatter().setWidth(0, "20%");
	    grid.getColumnFormatter().setWidth(1, "20%");
	    grid.getColumnFormatter().setWidth(2, "60%");
	    grid.setCellPadding(2);
	    grid.setWidget(0, 0, initialDecorator);
	    grid.setWidget(0, 1, fixedDecorator);
	    grid.setWidget(0, 2, textAreaDecorator);

	    // Wrap the trees in DecoratorPanels
	    RootPanel.get("astTreeView").add(grid);
	}

	public void updateASTTreeView(RegEx regEx) {
		_initalAST = createTreeView(regEx);
		_initialTreeWrapper.clear();
		_initialTreeWrapper.add(_initalAST);
	}
	
	public void updateFixedASTTreeView(RegEx regEx) {
		_fixedAST = createTreeView(regEx);
		_fixedTreeWrapper.clear();
		_fixedTreeWrapper.add(_fixedAST);
	}
	
	private Tree createTreeView(RegEx regEx) {
	    Tree staticTree = new Tree();
	    TreeItem treeItem = staticTree.addTextItem("Start");
	    createRecursiveTree(treeItem, regEx);
	    return staticTree;
	}

	private TreeItem createRecursiveTree(TreeItem treeItem, RegEx regEx) {
		if (regEx instanceof Alternative) {
			TreeItem item = treeItem.addTextItem("+");
			createRecursiveTree(item, ((Alternative) regEx).getFirstAlt());
			createRecursiveTree(item, ((Alternative) regEx).getSecondAlt());
		} else if (regEx instanceof Concatenation) {
			if (((Concatenation) regEx).getFirst() instanceof Epsilon) {
				return createRecursiveTree(treeItem, ((Concatenation) regEx).getSecond());
			} else {
				TreeItem item = treeItem.addTextItem("&");
				createRecursiveTree(item, ((Concatenation) regEx).getFirst());
				createRecursiveTree(item, ((Concatenation) regEx).getSecond());
			}
		} else if (regEx instanceof Repetition) {
			TreeItem item = treeItem.addTextItem("*");
			createRecursiveTree(item, ((Repetition) regEx).getInternal());
		} else if (regEx instanceof Primitive) {
			TreeItem item = treeItem.addTextItem(String.valueOf((((Primitive) regEx).getCharacter())));
			return item;
		} else if (regEx instanceof Epsilon) {
			//nothing to do
		}
		return treeItem;
	}

	public TextBox getRegularExpressionTextBox() {
		return _regularExpressionTextBox;
	}

	public TextBox getWordTextBox() {
		return _wordTextBox;
	}

	public Label getMatches() {
		return _matches;
	}

	public TextBox getFixedTextBox() {
		return _fixedTextBox;
	}

	public TextArea getTextArea() {
		return _textArea;
	}

	public Image getImage() {
		return _image;
	}
}
