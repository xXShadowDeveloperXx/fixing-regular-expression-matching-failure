package hska.project.fix.regex.mf.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;

public class Controller implements EntryPoint, BlurHandler, KeyPressHandler {
	
	private MainView _mainView; 
	private FixingRegularExpressionMatchingFailure _model;
	
	@Override
	public void onModuleLoad() {
		Resources.INSTANCE.css().ensureInjected();
		_model = new FixingRegularExpressionMatchingFailure();
		_mainView = new MainView(this);
		_mainView.init();
	}

	@Override
	public void onBlur(BlurEvent event) {
		handleFix();
	}

	@Override
	public void onKeyPress(KeyPressEvent event) {
		if (event.getCharCode() == KeyCodes.KEY_ENTER) {
			handleFix();
		}
	}
	
	private void handleFix() {
		String regEx = _mainView.getRegularExpressionTextBox().getText();
		String word = _mainView.getWordTextBox().getText();
		if (!regEx.isEmpty() && !word.isEmpty()) {
			_mainView.updateASTTreeView(_model.parseRegex(regEx));
			_model.derive(word);
			_model.match();
			_mainView.getTextArea().setText(_model.printDerivatedSteps());
			_mainView.updateFixedASTTreeView(_model.fixRegularExpression());
			
			_mainView.getImage().setResource(_model.isMatch() ? Resources.INSTANCE.passedImage()
					: Resources.INSTANCE.errorImage());
			_mainView.getFixedTextBox().setText(_model.getFixedRegEx());
		}
	}
	
}
