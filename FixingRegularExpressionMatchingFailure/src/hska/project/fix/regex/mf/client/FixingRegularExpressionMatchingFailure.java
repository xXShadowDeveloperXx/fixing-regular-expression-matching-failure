package hska.project.fix.regex.mf.client;

import java.util.ArrayList;
import java.util.List;

import hska.project.fix.regex.mf.parser.Alternative;
import hska.project.fix.regex.mf.parser.Concatenation;
import hska.project.fix.regex.mf.parser.Epsilon;
import hska.project.fix.regex.mf.parser.Faulty;
import hska.project.fix.regex.mf.parser.Primitive;
import hska.project.fix.regex.mf.parser.RegEx;
import hska.project.fix.regex.mf.parser.RegExParser;
import hska.project.fix.regex.mf.parser.Repetition;

public class FixingRegularExpressionMatchingFailure {
	
	private RegEx _regTree;
	private List<RegEx> _regExList;
	private List<List<RegEx>> _listOfAllDerivateSteps;
	private boolean _match;
	private String _fixedRegEx;
	
	/**
	 * For TESTING!
	 * @param args
	 */
	public static void main(String[] args) {
		FixingRegularExpressionMatchingFailure fixRegEx = new FixingRegularExpressionMatchingFailure();
		fixRegEx.matchWordAgainsRegEx("asd", "dsd");
		System.out.println(fixRegEx.getFixedRegEx());
	}
	
	public void matchWordAgainsRegEx(String regEx, String word) {
		parseRegex(regEx);
		derive(word);
		match();
		printDerivatives();
		fixRegularExpression();
	}
	
	/**
	 * Parse regular expression and create an AST.
	 * @param regEx
	 * @return
	 */
	public RegEx parseRegex(String regEx) {
		RegExParser exParser = new RegExParser(regEx);
		_regTree =  exParser.parse();
		return _regTree;
	}
	/**
	 * Prepare and derivate AST.
	 * @param word
	 */
	public void derive(String word) {
		_listOfAllDerivateSteps = new ArrayList<>();
		_regExList = new ArrayList<>();
		_regExList.add(_regTree);
		
		//derive
		List<RegEx> tempRegExList = new ArrayList<>();
		List<RegEx> noDuplicatesRegExList = new ArrayList<RegEx>();
		for (int i = 0; i < word.length(); i++) {
			for (RegEx regEx : _regExList) {
				tempRegExList.addAll(regEx.derive(word.charAt(i)));
			}
			_regExList.clear();
			//remove duplicates
			noDuplicatesRegExList.add(tempRegExList.get(0));
			for (RegEx currentRegEx : tempRegExList) {
				boolean isInList = false;
				for (RegEx innerRegEx : noDuplicatesRegExList) {
					if (compareRegExTrees(currentRegEx, innerRegEx)) {
						isInList = true;
						break;
					}
				}
				if (!isInList) {
					noDuplicatesRegExList.add(currentRegEx);
				}
			}
			_regExList.addAll(noDuplicatesRegExList);
			_listOfAllDerivateSteps.add(new ArrayList<RegEx>(_regExList));
			tempRegExList.clear();
			noDuplicatesRegExList.clear();
			
			//Sind alle derivatives faulties kann abgebrochen werden
			int numberOfFaulties = 0;
			List<RegEx> temp = _listOfAllDerivateSteps.get(_listOfAllDerivateSteps.size() - 1);
			for (RegEx regEx : temp) {
				if (regEx instanceof Faulty) {
					numberOfFaulties++;
				} else if (regEx instanceof Concatenation) {
					if (isFaultyFromConcatenation(regEx)) {
						numberOfFaulties++;
					}
				} else if (regEx instanceof Alternative) {
					if (((Alternative) regEx).getFirstAlt() instanceof Faulty
							&& ((Alternative) regEx).getSecondAlt() instanceof Faulty) {
						numberOfFaulties++;
					}
				}
			}
			if (numberOfFaulties == temp.size()) {
				break;
			}
		}
	}
	
	/**
	 * Matches the entered text with the regular expression.
	 */
	public void match() {
		_match = false;
		for (RegEx regEx : _regExList) {
			if (regEx.isNullable(regEx)) {
				_match = true;
				break;
			}
		}
		if (_match) {
			System.out.println("---- matches ----");
		} else {
			System.out.println("!!!! matches not !!!!");
		}
		System.out.println();
	}
	
	private void printDerivatives() {
		//print derivatives
		int step = 1;
		for (List<RegEx> steps : _listOfAllDerivateSteps) {
			System.out.println("----begin of " + step + ". step -----");
			for (RegEx regEx : steps) {
				regEx.print();
				System.out.println();
			}
			System.out.println("----end of " + step + ". step-----");
			System.out.println();
			step++;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public String printDerivatedSteps() {
		String output = "";
		int step = 1;
		output = "--- Begin of derivation ---\n";
		for (List<RegEx> steps : _listOfAllDerivateSteps) {
			output += "--- " + step + ". Step ---\n";
			for (RegEx regEx : steps) {
				output += regEx.printAsString();
				output += "\n";
			}
//			output += "--- " + step + ". Step end ---\n";
			step++;
		}
		output += "--- End of derivation ---\n";
		return output;
	}
	
	/**
	 * Fixes the regular expression.
	 */
	public RegEx fixRegularExpression() {
		_fixedRegEx = "";
		if (!_match) {
			for (RegEx part : _listOfAllDerivateSteps.get(_listOfAllDerivateSteps.size() -1)) {
				RegEx faulty = part;
				if (part instanceof Concatenation) {
					if (isFaultyFromConcatenation(part)) {
						faulty = extractFaultyFromConcatenation(part);
					}
				}
				if (faulty instanceof Faulty) {
					_regTree = fixRegEx(_regTree, faulty);
				}
			}
			_fixedRegEx = generateFixedRegExString(_regTree, "");
		}
		return _regTree;
	}
	
	private RegEx fixRegEx(RegEx tree, RegEx faulty) {
		if (tree instanceof Alternative) {
			fixRegEx(((Alternative) tree).getFirstAlt(), faulty);
			fixRegEx(((Alternative) tree).getSecondAlt(), faulty);
		} else if (tree instanceof Concatenation) {
			RegEx first = ((Concatenation) tree).getFirst();
			if (!(first instanceof Epsilon)) {
				fixRegEx(first, faulty);
			}
			((Concatenation) tree).setSecond(fixRegEx(((Concatenation) tree).getSecond(), faulty));
			
		} else if (tree instanceof Repetition) {
			((Repetition) tree).setInternal(fixRegEx(((Repetition) tree).getInternal(), faulty));
		} else if (tree instanceof Primitive) {
			Faulty tempFaulty;
			if (faulty instanceof Concatenation) {
				tempFaulty = extractFaultyFromConcatenation(((Concatenation) faulty).getFirst());
			} else {
				tempFaulty = (Faulty) faulty;
			}
			if (((Primitive) tree).getLabel() == tempFaulty.getLabel()) {
				if (tempFaulty.getFound() instanceof Primitive) {
					//Primitive fix
					Concatenation firstConcatenation = new Concatenation(new Epsilon(), tree);
					Concatenation secondConcatenation = new Concatenation(new Epsilon(), new Primitive(tempFaulty.getExpected(), 0));
					Alternative alternative = new Alternative(firstConcatenation, secondConcatenation);
					tree  = alternative;
				} else {
					//Epsilon fix
					Concatenation leftConcatenation = new Concatenation(new Epsilon(), tree);
					Concatenation innerConcatenation = new Concatenation(new Epsilon(), tree);
					Concatenation rightConcatenation = new Concatenation(innerConcatenation, new Primitive(tempFaulty.getExpected(), 0));
					Alternative alternative = new Alternative(leftConcatenation, rightConcatenation);
					tree = alternative;
				}
			}
		} 
		return tree;
	}
	
	private String generateFixedRegExString(RegEx fixedTree, String fixedRegEx) {
		if (fixedTree instanceof Alternative) {
			String alternative = "(";
			alternative = alternative + generateFixedRegExString(((Alternative) fixedTree).getFirstAlt(), fixedRegEx);
			alternative = alternative + "+";
			alternative = alternative + generateFixedRegExString(((Alternative) fixedTree).getSecondAlt(), fixedRegEx);
			alternative = alternative + ")";
			fixedRegEx = fixedRegEx + alternative;
		} else if (fixedTree instanceof Concatenation) {
			String concatenation = "";
			RegEx first = ((Concatenation) fixedTree).getFirst();
			if (!(first instanceof Epsilon)) {
				concatenation = concatenation + generateFixedRegExString(first, fixedRegEx);
			}
			concatenation = concatenation + generateFixedRegExString(((Concatenation) fixedTree).getSecond(), fixedRegEx);
			fixedRegEx = fixedRegEx + concatenation;
		} else if (fixedTree instanceof Repetition) {
			String repetition = "(";
			repetition = repetition + generateFixedRegExString(((Repetition) fixedTree).getInternal(), fixedRegEx);
			repetition = repetition + ")*";
			fixedRegEx = fixedRegEx + repetition;
		} else if (fixedTree instanceof Primitive) {
			fixedRegEx = fixedRegEx + ((Primitive)fixedTree).getCharacter();
		} 
		return fixedRegEx;
	}
	
	private boolean compareRegExTrees(RegEx firstTree, RegEx secondTree) {
		if ((firstTree instanceof Concatenation) && (secondTree instanceof Concatenation)) {
			Concatenation firstTreeConcatenation = (Concatenation) firstTree;
			Concatenation secondTreeConcatenation = (Concatenation) secondTree;
			return compareRegExTrees(firstTreeConcatenation.getFirst(), secondTreeConcatenation.getFirst()) 
					&& compareRegExTrees(firstTreeConcatenation.getSecond(), secondTreeConcatenation.getSecond());
		} else if ((firstTree instanceof Repetition) && (secondTree instanceof Repetition)) {
			Repetition firstTreeRepetition = (Repetition) firstTree;
			Repetition secondTreeRepetition = (Repetition) secondTree;
			return compareRegExTrees(firstTreeRepetition.getInternal(), secondTreeRepetition.getInternal());
		} else if ((firstTree instanceof Primitive) && (secondTree instanceof Primitive)) {
			if (((Primitive) firstTree).getLabel() == ((Primitive) secondTree).getLabel()) {
				return true;
			}
		} else if ((firstTree instanceof Epsilon) && (secondTree instanceof Epsilon)) {
			if (((Epsilon) firstTree).getLabel() == ((Epsilon) secondTree).getLabel()) {
				return true;
			}
		} else if ((firstTree instanceof Faulty) && (secondTree instanceof Faulty)) {
			if (((Faulty) firstTree).getLabel() == ((Faulty) secondTree).getLabel()) {
				return true;
			}
		}
		return false;
	}
	
	private Faulty extractFaultyFromConcatenation(RegEx concatenation) {
		if (concatenation instanceof Faulty) {
			return (Faulty) concatenation;
		} else {
			Concatenation firstConcatenation = (Concatenation) concatenation;
			if (firstConcatenation.getFirst() instanceof Concatenation) {
				return extractFaultyFromConcatenation((Concatenation) firstConcatenation.getFirst());
			} else {
				return (Faulty) ((Concatenation) concatenation).getFirst();
			}
		}
	}
	
	private boolean isFaultyFromConcatenation(RegEx concatenation) {
		if (concatenation instanceof Faulty) {
			return true;
		} else {
			Concatenation firstConcatenation = (Concatenation) concatenation;
			if (firstConcatenation.getFirst() instanceof Concatenation) {
				return isFaultyFromConcatenation((Concatenation) firstConcatenation.getFirst());
			} else if (((Concatenation) concatenation).getFirst() instanceof Faulty) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public String getFixedRegEx() {
		return _fixedRegEx;
	}

	public boolean isMatch() {
		return _match;
	}
}
