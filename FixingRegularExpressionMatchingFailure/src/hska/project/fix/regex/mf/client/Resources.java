package hska.project.fix.regex.mf.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;

public interface Resources extends ClientBundle {

	 public static final Resources INSTANCE = GWT.create(Resources.class); 

     @Source("Projektarbeit.css")
     @CssResource.NotStrict
     CssResource css();
     
     @Source("passed.png")
     ImageResource passedImage();
     
     @Source("error.png")
     ImageResource errorImage();
}
