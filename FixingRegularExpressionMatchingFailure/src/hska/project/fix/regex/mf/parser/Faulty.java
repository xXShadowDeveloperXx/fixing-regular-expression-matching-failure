package hska.project.fix.regex.mf.parser;

import java.util.ArrayList;
import java.util.List;

public class Faulty extends RegEx{
	private char _expected;
	private RegEx _found;
	
	public Faulty(char expected, RegEx found) {
		_expected = expected;
		_found = found;
	}

	@Override
	public List<RegEx> derive(char character) {
		List<RegEx> list = new ArrayList<RegEx>();
		list.add(this);
		return list;
	}

	@Override
	public void print() {
		System.out.print("(");
		System.out.print(_expected);
		System.out.print("|--");
		_found.print();
		System.out.print("--)");
	}
	
	@Override
	public String printAsString() {
		return "(" + _expected + "|--" + _found.printAsString() + "--)";
	}
	
	public char getExpected() {
		return _expected;
	}
	
	public int getLabel() {
		if (_found instanceof Primitive) {
			return ((Primitive) _found).getLabel();
		} else {
			return ((Epsilon) _found).getLabel();
		}
	}

	public RegEx getFound() {
		return _found;
	}
}
