package hska.project.fix.regex.mf.parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Concatenation extends RegEx {

	private RegEx _first;
	private RegEx _second;
	
	public Concatenation(RegEx first, RegEx second) {
		_first = first;
		_second = second;
	}

	@Override
	public List<RegEx> derive(char character) {
		List<RegEx> regEx;
		List<RegEx> returnValue = new ArrayList<RegEx>();
//		if (!(_first instanceof Epsilon) && !(_first instanceof Faulty)) {
		if (!(_first instanceof Epsilon)) {
			if (isNullable(_first)) {
				
				regEx = _first.derive(character);
				RegEx smartRegEx = smartConcatinate(regEx.get(0), _second);
				returnValue.add(smartRegEx);
				
				regEx = _second.derive(character);
				returnValue.addAll(regEx);
				return returnValue;
			} else {
				regEx = _first.derive(character);
				RegEx smartRegEx = smartConcatinate(regEx.get(0), _second);
				returnValue = new ArrayList<RegEx>();
				returnValue.add(smartRegEx);
				return returnValue;
			}
		}
//		if (!(_second instanceof Faulty)) {
			regEx = _second.derive(character);
			returnValue.addAll(regEx);
			return returnValue;
//		} else {
//			Epsilon tempEpsilon = new Epsilon();
//			returnValue = tempEpsilon.derive(character);
//			return returnValue;
//		}
	}
	
	@Override
	public void print() {
		if (!(_first instanceof Epsilon)) {
			_first.print();
			System.out.print(".");
			_second.print();
		} else {
			_second.print();
		}
	}
	
	@Override
	public String printAsString() {
		if (!(_first instanceof Epsilon)) {
			return _first.printAsString() + "." + _second.printAsString();
		} else {
			return _second.printAsString();
		}
	}
	
	public RegEx getFirst() {
		return _first;
	}

	public void setFirst(RegEx first) {
		_first = first;
	}

	public RegEx getSecond() {
		return _second;
	}

	public void setSecond(RegEx second) {
		_second = second;
	}
}
