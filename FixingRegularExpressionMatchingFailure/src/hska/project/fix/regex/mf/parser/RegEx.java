package hska.project.fix.regex.mf.parser;

import java.util.ArrayList;
import java.util.List;

public abstract class RegEx {
	
	public abstract List<RegEx> derive(char character);
	public abstract void print();
	public abstract String printAsString();
//	public abstract void fix(int label);
	
	public boolean isNullable(RegEx regEx) {
		if (regEx instanceof Epsilon) {
			return true;
		}
		if (regEx instanceof Primitive) {
			return false;
		}
		if (regEx instanceof Alternative) {
			Alternative regAlternative = (Alternative) regEx;
			return isNullable(regAlternative.getFirstAlt()) || isNullable(regAlternative.getSecondAlt());
		}
		if (regEx instanceof Concatenation) {
			Concatenation concatenation = (Concatenation) regEx;
			if (!(concatenation.getFirst() instanceof Epsilon)) {
				return isNullable(concatenation.getFirst()) && isNullable(concatenation.getSecond());
			} else {
				return isNullable(concatenation.getSecond());
			}
		}
		if (regEx instanceof Repetition) {
			return true;
		}
		return false;
	}
	
	public RegEx smartConcatinate(RegEx firstRegEx, RegEx secondRegEx) {
		if (firstRegEx instanceof Epsilon) {
			return secondRegEx;
		}
		if (secondRegEx instanceof Epsilon) {
			return firstRegEx;
		}
		return new Concatenation(firstRegEx, secondRegEx);
	}
	
	public List<RegEx> smartConcatinate(List<RegEx> firstRegEx, RegEx secondRegEx) {
		List<RegEx> list = new ArrayList<RegEx>();
		for (RegEx regEx : firstRegEx) {
			list.add(smartConcatinate(regEx, secondRegEx));
		}
		return list;
	}
}
