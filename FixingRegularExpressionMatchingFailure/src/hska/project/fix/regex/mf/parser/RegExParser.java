package hska.project.fix.regex.mf.parser;

public class RegExParser {
	
	private String _input;
	private RegEx _regex;
	private int _labelCounter;
	
	public RegExParser(String input) {
		_input = input;
	}
	
	public RegEx parse() {
		_labelCounter = 1;
		return regex();
	}
	
	private char peek() {
		return _input.charAt(0);
	}
	
	private void eat(char c) {
		if (peek() == c) {
			_input = _input.substring(1);
		} else {
			throw new RuntimeException("Expected: " + c + "; got: " + peek());
		}
	}
	
	private char next() {
		char c = peek();
		eat(c);
		return c;
	}
	
	private boolean more() {
		return _input.length() > 0;
	}
	

	
	private RegEx regex() {
		RegEx term = term();
		if (more() && peek() == '+') {
			eat('+');
			RegEx regex = regex();
			return new Alternative(term, regex);
		} else {
			return term;
		}
	}
	
	private RegEx term() {
		RegEx factor = new Epsilon();
		while(more() && peek() != ')' && peek() != '+') {
			RegEx nextFactor = factor();
			factor = new Concatenation(factor, nextFactor);
		}
		return factor;
	}
	
	private RegEx factor() {
		RegEx base = base();
		while(more() && peek() == '*') {
			eat('*');
			base = new Repetition(base);
		}
		return base;
	}
	
	private RegEx base() {
		switch(peek()) {
		case '(':
			eat('(');
			RegEx regex = regex();
			eat(')');
			return regex;
		case '\\':
			eat('\\');
			char esc = next();
			return new Primitive(esc, _labelCounter++);
		default:
			return new Primitive(next(), _labelCounter++);
		}
	}
}
