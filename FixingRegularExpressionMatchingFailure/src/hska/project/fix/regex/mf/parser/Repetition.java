package hska.project.fix.regex.mf.parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Repetition extends RegEx {

	private RegEx _internal;
	
	public Repetition(RegEx internal) {
		_internal = internal;
	}

	@Override
	public List<RegEx> derive(char character) {
		List<RegEx> list = new ArrayList<RegEx>();
		list.addAll(_internal.derive(character));
		
		List<RegEx> returnList = new ArrayList<>();
		returnList.addAll(smartConcatinate(list, this));
		return returnList;
	}
	
	@Override
	public void print() {
		System.out.print("(");
		_internal.print();
		System.out.print(")*");
	}
	
	@Override
	public String printAsString() {
		return "(" + _internal.printAsString() + ")*";
	}

	public RegEx getInternal() {
		return _internal;
	}

	public void setInternal(RegEx internal) {
		_internal = internal;
	}

}
