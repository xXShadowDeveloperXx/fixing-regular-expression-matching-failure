package hska.project.fix.regex.mf.parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Epsilon extends RegEx {
	
	private int _label;

	public Epsilon() {
		_label = -1;
	}
	
	public Epsilon(int label) {
		_label = label;
	}
	
	@Override
	public List<RegEx> derive(char character) {
		List<RegEx> list = new ArrayList<RegEx>();
		list.add(new Faulty(character, this));
		return list;
	}

	@Override
	public void print() {
		System.out.print("#EPSILON#" + _label);
	}
	
	public int getLabel() {
		return _label;
	}

	@Override
	public String printAsString() {
		return "#EPSILON#" + _label;
	}
}
