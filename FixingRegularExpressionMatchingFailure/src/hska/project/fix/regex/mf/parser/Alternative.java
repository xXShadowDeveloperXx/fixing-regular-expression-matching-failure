package hska.project.fix.regex.mf.parser;

import java.util.Collections;
import java.util.List;

public class Alternative extends RegEx {
	
	private RegEx _firstAlt;
	private RegEx _secondAlt;
	
	public Alternative(RegEx firstAlt, RegEx secondAlt) {
		_firstAlt = firstAlt;
		_secondAlt = secondAlt;
	}

	@Override
	public List<RegEx> derive(char character) {
		List<RegEx> firstList = _firstAlt.derive(character);
		List<RegEx> secondList = _secondAlt.derive(character);
		firstList.addAll(secondList);
		return firstList;
	}

	public RegEx getFirstAlt() {
		return _firstAlt;
	}

	public RegEx getSecondAlt() {
		return _secondAlt;
	}

	@Override
	public void print() {
		_firstAlt.print();
		System.out.print("+");
		_secondAlt.print();
	}

	@Override
	public String printAsString() {
		return _firstAlt.printAsString()
				+ "+" + _secondAlt.printAsString();
	}
}
